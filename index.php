<?php
    
    require_once('animal.php');
    require_once('ave.php');
    require_once('frog.php');
    
    
    $sheep = new Animal("Shaun");
    echo "Name : ". $sheep->name . "<br>"; // "shaun"
    echo "legs : ". $sheep->legs . "<br>"; // 4
    echo "cold blooded : ". $sheep->cold_blooded . "<br><br>"; // "no"
   
    $kodok = new Frog("buduk");
    echo "Name : ". $kodok->name . "<br>"; 
    echo "legs : ". $kodok->legs . "<br>"; 
    echo "cold blooded : ". $kodok->cold_blooded . "<br>";
    echo "Jump : ". $kodok->jump() . "<br><br>"; // "hop hop"

    $kera = new Ave("kera sakti");
    echo "Name : ". $kera->name . "<br>"; 
    echo "legs : ". $kera->legs . "<br>"; 
    echo "cold blooded : ". $kera->cold_blooded . "<br>";
    echo "Yell : " . $kera->Auooo() // "Auooo"
?>